import React from "react";
export default class Tags extends React.Component {
    constructor(props) {
        super(props);
        this.tags = props.tags;
        this.tagsToRender = this.tags.map((tag) => <div class="tag">#{tag}</div>)
    }
    render() {
        return (
            <div class="tags">{this.tagsToRender}</div>
        )
    }
}