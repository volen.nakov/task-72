import "./App.css";
import Tags from "./Tags";
function App() {
  return (
    <div className="App">
      <Tags tags={["boomdotdev", "task", "tags", "react"]} />
    </div>
  );
}

export default App;
